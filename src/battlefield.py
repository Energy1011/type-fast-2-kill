#!/usr/bin/env python
# coding=utf-8

"""
    Module: BattleField for typeFast2Kill
"""

from kivy.core.window import Window
from kivy.uix.widget import Widget
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from player import Player
from handstutor import HandsTutor
from imagepack import ImagePack
from enemy import Enemy
from random import randint

import g

class BattleField(Widget):
    """
        Battle Field class, it controls player, enemies, game logic, etc
    """
    player = Player()           # Player
    hands_tutor = HandsTutor()  # Hands Graphics help
    enemy_list = []             # All enemies
    target_enemy = None         # Current target enemy
    images = ImagePack()

    def __init__(self, **kvargs):
        super(BattleField, self).__init__(**kvargs)
        self.size = Window.size
        self.images.load_map('map_1.png')
        self.add_widget(self.images.packs['map'])
        self.add_widget(self.hands_tutor)
        self.add_widget(self.player)
        self.main_textinput = TextInput(font_size=20,
            size_hint_y=None,
            height=40,text=" ")
        self.main_textinput.bind(text=self.get_typing)
        self.add_widget(self.main_textinput)
        self.label_time = Label(text="0")
        self.add_widget(self.label_time)
        self.images.load_enemies() #Load sprites
        self.seconds = 0

    def remove_enemy(self, target = None):
        """
            Remove an enemy from battlefield (enemy mainlist)
        """
        if self.target_enemy == None:
            return
        else:
            if target == None:
                target = self.target_enemy

            # Remove its children widgets (label etc)
            self.enemy_list[target].clear_widgets(children=None)
            # Clear canvas
            self.enemy_list[self.target_enemy].canvas.clear()
            #Remove enemy object from enemy main list
            del self.enemy_list[self.target_enemy]
            print ("Target Enemy Killed !!")

    def spawn_enemy(self, name = False):
        """
            Create an enemy and add to (enemy main list)
        """
        # Get random type of enemy if name is False
        if name == False:
            name = list(g.enemies.keys())[randint(0,len(g.enemies)-1)]

        # Get random word with translation if exist
        word = g.game.wordset.get_random_word(0) #(Energy1011) TODO: load multi lists

        # Create enemy
        enemy = Enemy(random_text = word[0], random_text_translation = word[1], initial_sprite=self.images.packs[name][0], name=name)
        self.add_widget(enemy)
        self.enemy_list.append(enemy)

        self.target_enemy = 0

    def update_game_logic(self):
        """
            Update and check all game logic
        """
        # Energy1011) TODO: check difficult lvl and time playing for this stage
        # Are there enemies?
        if len(self.enemy_list) <= 0:
            self.spawn_enemy()

    def update_graphics(self):
        """
            Update all graphics in game: enemies, hands tutor, etc
        """
        # Move enemies
        for enemy in self.enemy_list:
            #Check for battlefield's boundaries
            if enemy.x <= 370:
                self.player.decrease_hp(5)
                #Check if player dies
                if self.player.is_dead():
                    g.game.state = 4
                # enemy.x = 2800
                continue

            # Next frame sprite
            if enemy.last_frame >= len(self.images.packs[enemy.name]):
                enemy.last_frame = 0
            enemy.actual_sprite = self.images.packs[enemy.name][enemy.last_frame]

            # Walk new position enemy
            enemy.x= enemy.x - 10
            enemy.label.x= enemy.x - 20
            enemy.last_frame = enemy.last_frame + 1

        # Draw current finger indicador
        self.hands_tutor.draw_indicator(self.enemy_list[self.target_enemy])

    def update_game(self, data):
        """
            It calls update game logic and graphics
        """
        # is playing ?
        if g.game.state == 1:
            self.update_game_logic()
            self.update_graphics()

        #is timeout, finished game, win or lose ?
        # (Energy1011) TODO: set custom time for label
        if g.game.state == 4 or self.seconds >= g.game.duration:
            g.game.state = 2
            g.game.sc_results.load_results(self.player)
            g.sm.current = 'results'

    def get_typing(self, input, text):
        """
            Get typed text from the main text input and check if it is correct
            param: KivyWidget input InputText
            param: String text typed
        """
        text = text.encode('utf-8')
        # Unbind to clear textinput
        input.unbind(text=self.get_typing)
        input.text="" # Clear text
        # Bind again
        input.bind(text=self.get_typing)

        # Check if exist at least one enemy
        if len(self.enemy_list) <= 0:
            return

        target_enemy = self.enemy_list[self.target_enemy]

        # Check for correct typed
        if  target_enemy.check_typed(text):
            self.player.sum_typed(1)
            # Check for enemy killed in this shot
            if target_enemy.is_dead():
                random = randint(1,3) #normal level
                for x in range(1,random):
                    self.spawn_enemy()
                    pass
                self.player.sum_words_typed(1)
                self.remove_enemy(self.target_enemy)
                return
        else:
            self.player.sum_errors(1)

    def sum_seconds(self, data):
        if g.game.state == 1:
            self.seconds = self.seconds + 1
            self.label_time.text = str(self.seconds) + " Seconds."
            if self.seconds >= 60:
                g.game.state = 4

    def remove_enemy_wigets(self):
        """ Iter over all enemies to remove """
        for enemy in self.enemy_list:
            self.remove_widget(enemy)

    def init_battlefield(self):
        """ Init battlefield vars """
        self.main_textinput.text = ""
        self.seconds = 0
        self.remove_enemy_wigets()
        self.enemy_list = []
        self.target_enemy = None
        self.player.init_player()
        g.game.state = 1
        g.sm.current = "game"