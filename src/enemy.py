#!/usr/bin/env python
# coding=utf-8

"""
    Module: Enemy for typeFast2Kill
"""

from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.graphics import Rectangle, Color, Line
from random import randint


class Enemy(Widget):
    """
        Enemy class
    """
    last_frame = 0


    def __init__(self, **kvargs):
        super(Enemy, self).__init__()
        self.x = 1024
        self.y = randint(200,650)
        self.cursor_pos = 0 # Current cursor position for typing
        # Create label
        self.label = Label(text=kvargs["random_text"], font_size=20, markup=True)
        self.label.y = self.y + 40 # Over enemy's head
        self.label.x = self.x - 20
        self.actual_sprite = kvargs["initial_sprite"]
        self.name = kvargs["name"] # Enemy name and image pack name
        self.text_translation = kvargs["random_text_translation"]

        # Add label widget
        self.add_widget(self.label)
        with self.canvas:
            self.rect = Rectangle(pos=[self.x, self.y], size=[45, 70], texture=self.actual_sprite)
        self.bind(pos=self.update_canvas)

    def update_canvas(self, *args):
        self.rect.pos = self.pos
        self.rect.texture=self.actual_sprite

    def is_dead(self):
        """
            Check if enemy is dead, the text against cursor position
            return: Boolean
        """
        self.remove_markup()
        if self.cursor_pos >= len(self.label.text):
            # This enemy is dead
            self.put_markup()
            return True
        self.put_markup()
        return False

    def check_typed(self, symbol):
        """
            Check for a typed symbol against to the current cursor position in the main text
            return: Boolean
        """
        # Strip spaces
        if len(symbol) > 1:
            symbol = symbol.strip()

        self.remove_markup()

        if symbol == self.label.text[self.cursor_pos].encode('utf-8'):
            self.cursor_pos = self.cursor_pos + 1
            self.put_markup()
            return True
        else:
            self.put_markup()
            return False

    def remove_markup(self):
        """
            Remove Kivy markup text
        """
        self.label.text = self.label.text.replace("[color=ff3333]","")
        self.label.text = self.label.text.replace("[/color]","")

    def put_markup(self):
        """
            Put Kivy markup text
        """
        self.label.text = "[color=ff3333]"+self.label.text[:self.cursor_pos]+"[/color]"+self.label.text[self.cursor_pos:len(self.label.text)]


    def get_current_symbol(self):
        """
            Get the enemy current symbol removing markup and getting cursor position
            return: Char
        """
        text = self.label.text.replace("[color=ff3333]","")
        text = text.replace("[/color]","")
        return text[self.cursor_pos]

    def get_label(self):
        return self.label