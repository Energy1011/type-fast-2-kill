#!/usr/bin/env python
# coding=utf-8

"""
Game for training typing
Copyright (C) 2016 4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from kivy.core.window import Window
from kivy.app import App

from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.graphics.instructions import InstructionGroup, Callback
from kivy.properties import ObjectProperty, ListProperty, StringProperty

import g # Globals

class TypeFast2Kill(App):
    """
        Main class to build and run the game
    """

    def build(self):
        # Window.fullscreen = True
        Window.size = (1024, 768)
        g.game.init_game()
        return g.sm

if __name__ == '__main__':
    TypeFast2Kill().run()