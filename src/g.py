"""
    Module: GLOBAL VARS
"""

#Global vars
path_data = "../data/"
path_sets = "../sets/"

from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.uix.screenmanager import Screen
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import ObjectProperty
from kivy.factory import Factory
from kivy.uix.popup import Popup
from battlefield import BattleField
from wordset import WordSet

import sys

# Main screen manager, menus and game screens
sm = ScreenManager()


# Enemies names and regions to get from textures (ImagePacks)
enemies = {"skeleton":(
                        (100, 125, 30, 50), #first frame
                        (133, 125, 30, 50),
                        (166, 125, 30, 50)
                      ),
            "minizombie":(
                        (0, 125, 30, 50),
                        (33, 125, 30, 50),
                        (66, 125, 30, 50)
                      )
           }

class Game (Widget):
    name = "TypeFast2Kill"
    version = "0.1"
    state = 2 # 1 = playing, 2 = pause, 3 = exit, 4 = timeout
    duration = 60 #segs
    wordset = ""
    wordset_path = "/home/energy/projects/type-fast-2-kill/sets/normal_1.txt"

    def __init__(self):
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)

    def exit(self):
        """
            Exit game system call exit
        """
        sys.exit()

    def pause(self):
        sm.current = 'menu'
        self.state = 2

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard = None

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if keycode[1] == 'escape' or keycode[0] == 27:
            self.pause()
        return True

    def start_schedule_update_game(self):
        #Update game
        Clock.schedule_interval(self.battle_field.update_game, 10 / 60.0)

    def start_schedule_sum_seconds(self):
        #Time counter
        Clock.schedule_interval(self.battle_field.sum_seconds, 60.0/60.0)

    def init_game(self):
        self.battle_field = BattleField()
        sm.add_widget(MenuScreen(name='menu'))
        self.sc_results = ResultsScreen(name='results')
        sm.add_widget(self.sc_results)
        sm.current = 'menu'
        self.sc_game = GameScreen(name='game')
        self.sc_game.add_widget(self.battle_field)
        sm.add_widget(self.sc_game)
        #start schedule
        self.start_schedule_sum_seconds()
        self.start_schedule_update_game()

    def start_game(self):
        self.battle_field.init_battlefield()
        pass

# Screens
class ResultsScreen(Screen):

    def goto_main_menu(self):
        sm.current = 'menu'

    def start_game(self):
        game.start_game()

    def load_results(self, player):
        label = self.ids['label_message']
        btn = self.ids['btn_next']
        if player.hp <= 0:
            label.text = "You Lose :("
            btn.text = "Play again"
        else:
            label.text = "You Win :)"
            btn.text = "Play other level"

class LoadDialog(FloatLayout):
    """
        FileChooser dialog
    """
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)
    path = path_sets #default path wordset

class MenuScreen(Screen):

    def __init__(self, **kvargs):
        super(MenuScreen, self).__init__(**kvargs)

    def start_game(self):
        sm.current = 'game'
        game.state = 1

    def show_load(self):
        content = LoadDialog(load=self.load, cancel=self.dismiss_popup)
        self._popup = Popup(title="Load wordset file", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def dismiss_popup(self):
        self._popup.dismiss()

    def load(self, filename):
        if len(filename) <= 0:
            print ("Select a file.")
            return
        game.wordset_path = filename[0].encode('utf-8')
        game.wordset = WordSet()
        label_wordset_selected = self.ids['label_wordset_selected']
        label_wordset_selected.text = "WordSet Game Loaded: "+str(game.wordset_path)
        self.dismiss_popup()

    def exit_game(self):
        game.state = 3
        game.exit()


class GameScreen(Screen):
    pass

# Factory.register('LoadDialog', cls=LoadDialog)

#Global Game object
game = Game()
