#!/usr/bin/env python
# coding=utf-8

"""
    Module: ImagePacks for typeFast2Kill
"""

from kivy.uix.image import Image
from math import sqrt, log, e
from random import choice, random
import g


class ImagePack():
    """
        Image pack class
    """
    packs = {}

    def __init__(self, **kvargs):
        pass

    def load(self, file_name, pack_name, regions):
        # (Energy1011) TODO: complete this method to load x pack
        sprites = []
        pack = Image(source=g.path_data+pack_name)
        for x in xrange(0,len(regions)):
            #load X pack
            pass

    def load_enemies(self):
        pack = Image(source=g.path_data+'pack_enemies1.png')
        for enemy in g.enemies.keys():
            sprites= []
            g.enemies[enemy]
            for region in g.enemies[enemy]:
                sprites.append(pack.texture.get_region(*region)) #first sprite skeleton
            self.packs[enemy] = sprites

    def load_map(self, file_name):
        image = Image(source=g.path_data+file_name, size=(1024,768))
        self.packs['map'] = image