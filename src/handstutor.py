#!/usr/bin/env python
# coding=utf-8

"""
    Module: Handstutor for typeFast2Kill
"""

from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.label import Label
import g

class HandsTutor(FloatLayout):
    """
        Hands Tutor class, it controls and show hands on interface to guide the user when typing
    """
    def __init__(self, **kvargs):
        super(HandsTutor, self).__init__(**kvargs)
        self.show = True
        self.size = (200,200)

        # Img hands
        img_hands_bg = Image(source=g.path_data+'hands.png', size=(200,200))
        img_hands_bg.pos = (400,-35)
        img_hands_bg.size = (400,400)
        self.add_widget(img_hands_bg)

        # Label current symbol
        self.label_current_symbol = Label(text="")
        self.label_current_symbol.x = 500
        self.label_current_symbol.y = 5
        self.add_widget(self.label_current_symbol)

        # Label current enemy word
        self.label_current_word = Label(text="", font_size=20, markup=True)
        self.label_current_word.x = 400
        self.label_current_word.y = 50
        self.add_widget(self.label_current_word)

        # Label current enemy word translation
        self.label_current_word_translation = Label(text="lang", font_size=20, markup=True)
        self.label_current_word_translation.x = 400
        self.label_current_word_translation.y = 85
        self.add_widget(self.label_current_word_translation)

        # Indicator positions in fingers, Starting by the left hand
        self.ind_pos = ((305,5),(323,25),(345,30),(375,22),
                        (400,-20),
                        (400,-20),
                        (430,22),(457,30),(480,25),(500,5))

        # Finger maps to know what finger is the correct to use
            #Pairs means { 'symbol':'finger number'}
        self.fingers_map = {                                             #QWERTY SPANISH LAYOUT
                            "ª": 0, "!": 0, "\"": 1, "·": 2, "$": 3, "%": 3, "&": 6, "/": 6, "(": 7, ")": 8, "=": 9, "?":9, "¿":9,
                            "º": 0, "1": 0, "2": 1, "3": 2, "4": 3, "5": 3, "6": 6, "7": 6, "8": 7, "9": 8, "0": 9, "'":9, "¡":9,
                                    "q": 0, "w": 1, "e": 2, "r": 3, "t": 3, "y": 6, "u": 6, "i": 7, "o": 8, "p": 9, "`":9, "+":9,"[":9, "]":9,"^":9, "*":9,
                                    "a": 0, "s": 1, "d": 2, "f": 3, "g": 3, "h": 6, "j": 6, "k": 7, "l": 8, "ñ": 9, "´":9, "ç":9,"{":9, "}":9,
                            "<": 0, ">": 0, "z": 0, "x": 1, "c": 2, "v": 3, "b": 3, "n": 6, "m": 6, ",": 7, ".": 8, "-": 9,
                                                                        " ": 4
                           }
        self.n_finger = 0

    def draw_indicator(self, enemy):
        """
            Draw indicador, a red circle indicating the correct finger to use
            param: Object enemy
        """
        if enemy == None:
            return
        # Update current symbol in label
        if enemy.get_current_symbol() == " ":
            self.label_current_symbol.text = "<Space> "
        else:
            self.label_current_symbol.text = enemy.get_current_symbol()
        # Show enemy label
        self.label_current_word.text = enemy.label.text
        # Show enemy label translation
        self.label_current_word_translation.text = "[color=51565B]"+enemy.text_translation+"[/color]"

        # Get finger number by symbol and check if exist in fingers_map dictionary
        if enemy.get_current_symbol().lower() in self.fingers_map != None:
            # Get current finger number
            current_n_finger = self.fingers_map[enemy.get_current_symbol().lower()]
            # Check if finger is not equal
            if self.n_finger != current_n_finger:
                self.n_finger = current_n_finger
                self.label_current_symbol.x = self.ind_pos[self.n_finger][0]
                self.label_current_symbol.y = self.ind_pos[self.n_finger][1]

        # Draw indicator example
        # with self.canvas:
        #     Color(1., 0, 0)
        #     self.rect = Line(circle=(405, 90, 7))