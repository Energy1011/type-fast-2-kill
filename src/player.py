#!/usr/bin/env python
# coding=utf-8

"""
    Module: Player for typeFast2Kill
"""

from kivy.uix.widget import Widget
from kivy.uix.progressbar import ProgressBar
from kivy.uix.label import Label

class Player(Widget):
    """
        Player class
    """
    hp = 100 # Health points
    n_errors = 0 # Errors counter
    n_typed = 0 # Correct typed counter
    n_words = 0 # Correct words typed counter

    def __init__(self, **kvargs):
        super(Player, self).__init__(**kvargs)

        # Progressbar hp
        self.progress_life = ProgressBar(max=self.hp)
        self.progress_life.value = 100
        self.progress_life.x = 450
        self.progress_life.y = -50
        self.add_widget(self.progress_life)

        #Label healpoints counter
        self.label_hp = Label(text="Life Bar")
        self.label_hp.x = 450
        self.label_hp.y = -40
        self.add_widget(self.label_hp)

        #Label errors counter
        self.label_n_errors = Label(text="Errors: "+str(self.n_errors))
        self.label_n_errors.x = 700
        self.label_n_errors.y = 10
        self.add_widget(self.label_n_errors)

        #Label typed counter
        self.label_n_typed = Label(text="Typed: "+str(self.n_typed))
        self.label_n_typed.x = 200
        self.label_n_typed.y = 10
        self.add_widget(self.label_n_typed)

        #Label correct words typed counter
        self.label_n_words = Label(text="Words Typed: "+str(self.n_typed))
        self.label_n_words.x = 200
        self.label_n_words.y = -10
        self.add_widget(self.label_n_words)

    def sum_words_typed(self, number):
        """
            Sum n_words counter and update label
        """
        self.n_words = self.n_words + number
        self.label_n_words.text = "Words Typed: "+str(self.n_words)

    def sum_errors(self, number):
        """
            Sum n_errors counter and update label
        """
        self.n_errors = self.n_errors + number
        self.label_n_errors.text = "Errors: "+str(self.n_errors)

    def sum_typed(self, number):
        """
            Sum n_typed counter and update label
        """
        self.n_typed = self.n_typed + number
        self.label_n_typed.text = "Typed: "+str(self.n_typed)

    def decrease_hp(self, number):
        """ Decrease heal points """
        self.hp = self.hp - number
        self.progress_life.value = self.progress_life.value - number


    def is_dead(self):
        """ Check if player is dead """
        return (self.hp <= 0)

    def init_player(self):
        """ Init player vars """
        self.hp = 100
        self.n_errors = 0
        self.n_typed = 0
        self.n_words = 0
        self.progress_life.value = 100
