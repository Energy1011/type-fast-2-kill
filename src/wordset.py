#!/usr/bin/env python
# coding=utf-8

"""
    Module: Wordset for typeFast2Kill
"""

import g
import os.path
import codecs
import re
from random import randint

class WordSet():
    """
        Word set class, it loads the word sets to play
    """
    sets_list = [] #Sets of words to play
    sets_list_translate = [] #Set of words to play translate

    def __init__(self, **kvargs):
        self.create_and_append()
        pass

    def create_and_append(self):
        """
            Load a txt file with words and apped the file into sets_list[]
        """
        file = g.game.wordset_path
        if os.path.isfile(file) is False:
            print ("File doesn't exist ",file)
            # (Energy1011) TODO: handle exception

        with codecs.open(file,'r',encoding='utf8') as f:
            set_list = []
            set_list_translate = []
            # iter over words
            for line in f:
                if line != "":
                    regx_res = None
                    regx_res = re.match(".*<l>(.*)</l>",line)
                    if regx_res:
                        # get lang word btwn special labels
                        set_list_translate.append(regx_res.group(1))
                        #remove lang special labels from line
                        line = re.sub("<l>.*</l>", "", line)
                    else:
                        #append empty word translation into the list
                        set_list_translate.append("")

                    set_list.append(line.rstrip('\n'))
            # check for min number of words to play
            if len(set_list) <= 1:
                print ("Number of words in file is <= 1, you need a wordset txt file with more than 1 line.")
            #(Energy1011) TODO: remove this inits and load multi lists
            self.sets_list = []
            self.sets_list_translate = []

            self.sets_list.append(set_list)
            self.sets_list_translate.append(set_list_translate)
        f.close()
        pass

    def get_random_word(self, set_number = 0):
        """
            Get a random list from the sets_list[] and sets_list_translate[] loaded
            param: Integer set_number set of words number txt file
            return: Array [0] = original word, [1] = translate word (if exists)
        """
        rand_n = randint(0, len(self.sets_list[set_number]) - 1)
        word = []
        word.append(self.sets_list[set_number][rand_n])
        word.append(self.sets_list_translate[set_number][rand_n])
        return word
